Parsdeo
========

Parse YouTube or Vimeo video ids from string in PHP and generate embed code. Parsdeo is easy to extend, just add a new driver to a *private* field in a class in *my* source code:

    private $drivers = array(
            'youtube' => array(
                'regexps' => array(
                    // http://www.youtube.com/watch?v=V8CQjV8vXJE
                    "/http\:\/\/www\.youtube\.com\/watch\?v\=([A-Za-z0-9\_\-]+)/",
                    // //www.youtube.com/embed/tdBH94hV2Js
                    "/\/\/www\.youtube\.com\/embed\/([A-Za-z0-9\_\-]+)/",
                ),
                'embed_method' => 'get_youtube_embed_code'
            ),

Example
--------

    $p = new Parsdeo("<a>http://www.youtube.com/watch?v=V8CQjV8vXJE</a><span>http://www.youtube.com/watch?v=q_oo0BPkozI</p> Example text
                      Example text http://vimeo.com/83968925 Example text
                      <a>http://www.vevo.com/watch/GB1101301014 http://www.pokec.sk/random/tags/?a
                      <iframe width=\"560\ height=\315\ src=\//www.youtube.com/embed/tdBH94hV2Js\" frameborder=\"0\" allowfullscreen></iframe>",
                     $settings = array('width' => 560, 'height' => 315));

    var_dump($p->parse());

    array(4) {
      [0]=>
      array(4) {
        ["driver"]=>
        string(7) "youtube"
        ["id"]=>
        string(11) "V8CQjV8vXJE"
        ["url"]=>
        string(42) "http://www.youtube.com/watch?v=V8CQjV8vXJE"
        ["embed_code"]=>
        string(116) "<iframe width="560" height="315" src="//www.youtube.com/embed/V8CQjV8vXJE" frameborder="0" allowfullscreen></iframe>"
      }
      [1]=>
      array(4) {
        ["driver"]=>
        string(7) "youtube"
        ["id"]=>
        string(11) "q_oo0BPkozI"
        ["url"]=>
        string(42) "http://www.youtube.com/watch?v=q_oo0BPkozI"
        ["embed_code"]=>
        string(116) "<iframe width="560" height="315" src="//www.youtube.com/embed/q_oo0BPkozI" frameborder="0" allowfullscreen></iframe>"
      }
      [2]=>
      array(4) {
        ["driver"]=>
        string(7) "youtube"
        ["id"]=>
        string(11) "tdBH94hV2Js"
        ["url"]=>
        string(35) "//www.youtube.com/embed/tdBH94hV2Js"
        ["embed_code"]=>
        string(116) "<iframe width="560" height="315" src="//www.youtube.com/embed/tdBH94hV2Js" frameborder="0" allowfullscreen></iframe>"
      }
      [3]=>
      array(4) {
        ["driver"]=>
        string(5) "vimeo"
        ["id"]=>
        string(8) "83968925"
        ["url"]=>
        string(25) "http://vimeo.com/83968925"
        ["embed_code"]=>
        string(203) "<iframe src="//player.vimeo.com/video/83968925?title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
      }
    }
