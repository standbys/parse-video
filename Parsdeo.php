<?php

/**
 * Copyright 2013 Michal Bystricky
 *
 * This file is part of Parsdeo.
 *
 * Parsdeo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Parsdeo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Parsdeo.  If not, see <http://www.gnu.org/licenses/>.
 */

class Parsdeo {

    private $drivers = array(
        'youtube' => array(
            'regexps' => array(
                // http://www.youtube.com/watch?v=V8CQjV8vXJE
                "/http\:\/\/www\.youtube\.com\/watch\?v\=([A-Za-z0-9\_\-]+)/",
                // //www.youtube.com/embed/tdBH94hV2Js
                "/\/\/www\.youtube\.com\/embed\/([A-Za-z0-9\_\-]+)/",
            ),
            'embed_method' => 'get_youtube_embed_code'
        ),
        'vimeo' => array(
            'regexps' => array(
                // http://vimeo.com/83968925
                "/http\:\/\/vimeo\.com\/([0-9]+)/"
            ),
            'embed_method' => 'get_vimeo_embed_code'
        )
    );

    public $urls = array();
    public $settings = array(
        'width' => 560,
        'height' => 315
    );

    public function Parsdeo($text_with_urls,
                           $settings = null) {

        preg_match_all(
            "/((http|https|ftp|ftps)?(\:\/\/|\/\/){1}(www\.)?[A-Za-z0-9\.\/\_\-]+(\?)?[A-Za-z0-9\&\=\_\-]*)/",
            $text_with_urls,
            $this->urls);

        $this->urls = $this->urls[0];

        if ($settings !== null)
            $this->settings = $settings;


    }

    public function parse() {

        $drivers_ids = array();
        $unmatched = array();

        foreach ($this->urls as $url) {
            $url_matched = false;
            foreach ($this->drivers as $driver_name => $driver) {
                foreach ($driver['regexps'] as $regexp) {
                    $is_matched = preg_match($regexp, $url, $matched);
                    if (!array_key_exists($driver_name, $drivers_ids))
                        $drivers_ids[$driver_name] = array();
                    if ($is_matched) {
                        $drivers_ids[$driver_name][] = array(
                            'driver' => $driver_name,
                            'id' => $matched[1],
                            'url' => $url
                        );
                        $url_matched = true;
                        break;
                    }
                }
                if ($url_matched) break;
            }
            if (!$url_matched) $unmatched[] = $url;
        }

        $videos = array();

        foreach ($drivers_ids as $driver_name => $videos_arr) {
            foreach ($videos_arr as $video) {
                $embed_method = $this->drivers[$driver_name]['embed_method'];
                $video['embed_code'] = $this->$embed_method($video['id'], $this->settings['width'], $this->settings['height']);
                $videos[] = $video;
            }
        }

        return $videos;
    }

    public function get_youtube_embed_code($id, $width, $height) {
        return "<iframe width=\"$width\" height=\"$height\" src=\"//www.youtube.com/embed/$id\" frameborder=\"0\" allowfullscreen></iframe>";
    }

    public function get_vimeo_embed_code($id, $width, $height) {
        return "<iframe src=\"//player.vimeo.com/video/$id?title=0&amp;byline=0&amp;portrait=0&amp;badge=0\" width=\"$width\" height=\"$height\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
    }

}
